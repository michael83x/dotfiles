(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(require 'use-package)

;; install and enable Evil mode
(use-package evil
    :ensure t
    :init
    (setq evil-want-C-u-scroll t) ; enable C-u scrolling
    (setq evil-want-keybinding nil)
    :config
    (evil-mode 1))

;; Optional: Install and enable Evil Collection for additional keybindings
(use-package evil-collection
    :after evil
    :ensure t
    :config
    (evil-collection-init))

;;DashBoard
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook))
(setq dashboard-banner-logo-title "Welcome to Emacs Dashboard")
(setq dashboard-startup-banner 'logo)
(recentf-mode 1)
(setq-default
 recentf-max-saved-items 100
 recentf-exclude '("/tmp/" "/ssh:" "/sudo:" "/TAGS$" "/var/folders/"))
(setq dashboard-items '((recents  . 10)
                        (bookmarks . 10)))
(setq initial-buffer-choice (lambda () (get-buffer "*dashboard*")))
(add-hook 'dashboard-mode-hook (lambda () (display-line-numbers-mode -1)))
(add-hook 'after-init-hook 'dashboard-setup-startup-hook)
(add-hook 'after-init-hook (lambda () (dashboard-refresh-buffer)))
;; Content is not centered by default. To center, set
;;(setq dashboard-center-content t)
;; To disable shortcut "jump" indicators for each section, set
;;(setq dashboard-show-shortcuts nil)

;;statusbar
(use-package doom-modeline
  :ensure t)
(doom-modeline-mode 1)
(setq inhibit-compacting-font-caches t)

;;Enable Which-Key
(use-package which-key
  :ensure t
  :config
  (which-key-mode))

;;Enables Python
(elpy-enable)
(setq elpy-rpc-python-command "python")

;;Doom Theme
(unless (package-installed-p 'doom-themes)
    (package-refresh-contents)
    (package-install 'doom-themes))
(require 'doom-themes)
(load-theme 'doom-one t)

;;
(require 'company)
(require 'company-emoji)
(add-to-list 'company-backends 'company-emoji)
(add-hook 'after-init-hook 'global-company-mode)
(set-fontset-font t 'symbol (font-spec :family "Noto Color Emoji") nil 'prepend)

;;
(use-package dired-single
  :ensure t
  :config
  (define-key dired-mode-map [return] 'dired-single-buffer)
  (define-key dired-mode-map [mouse-1] 'dired-single-buffer-mouse)
  (define-key dired-mode-map "^"
    (function
     (lambda nil (interactive) (dired-single-buffer "..")))))

;;Highlight color codes
(use-package rainbow-mode
  :ensure t
  :hook (prog-mode . rainbow-mode))

;;Suggestions
(use-package ivy
  :ensure t
  :config
  (ivy-mode 1))

;;
(global-hl-line-mode 1)

;; Set the default cursor style to a bar
(setq-default cursor-type 'bar)

;; Enable line numbers
(global-display-line-numbers-mode t)

;; Set the default font
(set-frame-font "JetBrainsMono Nerd Font 14" nil t)

;; Needed if using emacsclient. Otherwise, your fonts will be smaller than expected.
(add-to-list 'default-frame-alist '(font . "JetBrainsMono Nerd Font 14"))

;; Set the default tab width to 4 spaces
(setq-default tab-width 4)

;; Enable auto-indentation
(define-key global-map (kbd "RET") 'newline-and-indent)

;; Set up backup and autosave files
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq auto-save-file-name-transforms `((".*" "~/.emacs.d/auto-save-list/" t)))

;; Enable electric-pair-mode for automatic pairing of delimiters
(electric-pair-mode t)

;; Bind Ctrl-h to backward-delete-char instead of help
(define-key key-translation-map (kbd "C-h") (kbd "<DEL>"))

;; Enable Ctrl-x Ctrl-j for dired-jump
(require 'dired-x)

;; Set the default shell to bash
(setq-default shell-file-name "/bin/bash")

;; Set the default encoding to UTF-8
(prefer-coding-system 'utf-8)
(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)

;; Set the default browser to Firefox
(setq-default browse-url-browser-function 'browse-url-firefox)

;; Set the default search engine to Google
(setq-default engine/browser-function 'engine/search-google)

;; Use ibuffer instead of the default buffer list
(global-set-key (kbd "C-x C-b") 'ibuffer)

;; Bind Ctrl-x Ctrl-k to kill-this-buffer
(global-set-key (kbd "C-x C-k") 'kill-this-buffer)

;; Bind Ctrl-x b to switch-to-buffer
(global-set-key (kbd "C-x b") 'switch-to-buffer)

;; Bind Ctrl-x k to kill-buffer
(global-set-key (kbd "C-x k") 'kill-buffer)

;;zoom in/out
(global-set-key (kbd "C-=") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)

;;Disable menubar, toolbar, scrollbar
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;;remember last line
(save-place-mode 1)
(setq-default save-place t)

;;Enable icons
(use-package nerd-icons-dired
  :hook
  (dired-mode . nerd-icons-dired-mode))
(add-hook 'dired-mode-hook 'diredfl-mode)
(setq dired-listing-switches "-alh --group-directories-first")

;;Company for python
(add-hook 'after-init-hook 'global-company-mode)
;;(add-hook 'python-mode-hook #'lsp-deferred)
(setq lsp-python-ms-executable "python-language-server")

;;disable line-numbers inside dired/package-list/term/
(add-hook 'dired-mode-hook (lambda () (display-line-numbers-mode -1)))
(add-hook 'package-menu-mode-hook (lambda () (display-line-numbers-mode -1)))
(add-hook 'term-mode-hook (lambda () (display-line-numbers-mode -1)))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(rainbow-mode diredfl nerd-icons-dired dired-single which-key use-package lsp-jedi fontawesome evil-collection elpy doom-themes doom-modeline dashboard company-emoji)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
