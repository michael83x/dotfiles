#!/bin/sh

kitty -e vim $(find ~ | sed -e "s/'/\\\'/g" -e 's/\ /\\ /g' | sort -f | dmenu -i -p 'Find:' -l 10 -fn Monospace-14 -nb '#2e3440' -sb '#4c566a' -nf '#bbccdd')
