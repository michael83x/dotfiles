call plug#begin()
	Plug 'preservim/nerdtree'
	Plug 'vwxyutarooo/nerdtree-devicons-syntax'
	Plug 'ryanoasis/vim-devicons'
	Plug 'tiagofumo/vim-nerdtree-syntax-highlight'
	Plug 'sheerun/vim-polyglot'
	Plug 'vim-syntastic/syntastic'
	Plug 'nvie/vim-flake8'
	Plug 'navarasu/onedark.nvim'
	Plug 'nordtheme/vim'
	Plug 'morhetz/gruvbox'
	Plug 'sainnhe/everforest'
call plug#end()

let g:airline_theme="nord"
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#formatter = 'default'
let g:NERDTreeDirArrowExpandable = '+'
let g:NERDTreeDirArrowCollapsible = '-'
let NERDTreeShowHidden = 1
let NERDTreeMinimalUI = 1
let g:NERDTreeWinSize = 32
let g:NERDTreeHighlightCursorline = 1
let python_highlight_all = 1
let NERDTreeQuitOnOpen = 1
let &t_ut=''

set nocompatible
set number
syntax on
set tabstop=4
set shiftwidth=4
set expandtab
set hlsearch
set wildmenu
set wildoptions=pum
set encoding=utf-8
set t_Co=256
set termguicolors
set cursorline
set mouse=a
set clipboard=unnamedplus
set foldmethod=indent
hi Normal ctermbg=NONE guibg=NONE
set background=dark
colorscheme nord

au BufNewFile,BufRead *.py
    \set tabstop=4
    \set softtabstop=4
    \set shiftwidth=4
    \set textwidth=79
    \set expandtab
    \set autoindent
    \set fileformat=unix

" Return to last edit position when opening files (You want this!)
autocmd BufReadPost *
     \ if line("'\"") > 0 && line("'\"") <= line("$") |
     \   exe "normal! g`\"" |
     \ endif

nnoremap <F5> :NERDTreeToggle<CR>
nnoremap <C-s> :w<CR>
nnoremap <space> za

" Exit Vim if NERDTree is the only window remaining in the only tab.
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif
