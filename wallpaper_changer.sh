#!/bin/bash

while true; do
	feh --bg-fill --randomize ~/Pictures/*
	sleep 180s
done
